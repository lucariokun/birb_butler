from telethon.sync import TelegramClient, events
from telethon.tl.custom import Button
from telethon.tl.types import PeerUser, PeerChannel
from telethon import utils
from datetime import datetime, timedelta
from pytz import timezone
import json
import sqlite3
import random

###############################################################################
#                   Functions definition
###############################################################################


def readFile(f='misc.json'):
    with open(f, 'r') as fh:
        data = json.load(fh)
    return data


def storeMsg(messageObj):
    from_id = messageObj.from_id
    text = messageObj.text
    date = messageObj.date.astimezone(
        timezone('Europe/Rome')).strftime('%d-%m-%Y %H:%M:%S')
    query = 'INSERT INTO messages (from_id, text, date) VALUES (?, ?, ?)'
    db.execute(query, (from_id, text, date))
    conn.commit()

###############################################################################
#                   Configuration and initialization
###############################################################################


# Open config files to fetch various required parameters to start the bot.
config = readFile('config.json')

# Connect to the database and create a cursor.
conn = sqlite3.connect('database.db')
db = conn.cursor()

# Chats IDs
id_lega = config['id_chat']
id_legachan = config['id_chan']

# Other settings
admins = config['admins']
owner = config['owner']

# Initialize bot.
bot = TelegramClient('bot', config['api_id'], config['api_hash']).start(
    bot_token=config['bot_token'])

###############################################################################
#                   Callbacks for inline buttons
###############################################################################


@bot.on(events.CallbackQuery)
async def callback(event):
    original_message = await event.get_message()
    timediff = (datetime.now(timezone('Europe/Rome')) -
                original_message.date.astimezone(timezone('Europe/Rome'))).seconds / 60
    if timediff > 120:
        msg = 'Mi dispiace ma il messaggio a cui cerchi di rispondere è troppo vecchio, per favore richiama il menu con il comando /start'
        await event.edit(buttons=Button.clear())
        await event.respond(msg)
    else:

        cbdatadec = event.data.decode('utf-8').split('-')
        cbaction = cbdatadec[0]
        cbdata = int(cbdatadec[1]) if len(cbdatadec) > 1 else ''

        if cbaction == 'join':
            entity = await bot.get_entity(cbdata)

            msg = 'Certamente!\nPrima di farti entrare devo chiederti di rispondere ad alcune domande. Le risposte saranno visibili solamente agli admin del gruppo.'
            msg += '\nPrima di iniziare per favore prendi visione delle regole del gruppo e quando sei pronto premi il pulsante qui sotto!\n\n'
            msg += readFile()['rules']
            buttons = [Button.inline(
                'Fatto!', 'rdone-{}'.format(entity.id).encode())]

            query = 'SELECT accepted, name, username, registration_date FROM users WHERE user_id = ?'
            values = (entity.id,)
            db.execute(query, values)
            res = db.fetchone()

            if res:
                if res[0] == 1:
                    msg = 'Sei già stato accettato nel gruppo, hai forse smarrito la via?'
                    buttons = [
                        Button.inline(
                            'Sì :(', 'lost'.encode()),
                        Button.inline(
                            'No', 'notlost'.encode())
                    ]
                elif res[0] == 2:
                    msg = 'Sembra che tu sia stato bannato dal gruppo. Sono dispiaciuto ma non posso lasciarti entrare.'
                    buttons = Button.clear()
                else:
                    query = 'INSERT INTO try_logs (user_id, prev_name, prev_username, last_registration_attempt) VALUES (?, ?, ?, ?)'
                    values = (entity.id, res[1], res[2], res[3])
                    db.execute(query, values)

                    query = 'UPDATE users SET name = ?, username = ?, registration_date = ? WHERE user_id = ?'
                    values = (entity.first_name, entity.username,
                              datetime.now(timezone('Europe/Rome')).strftime('%d-%m-%Y %H:%M:%S'), entity.id)
                    db.execute(query, values)
                    conn.commit()
            else:
                query = 'INSERT INTO users (user_id, name, username, registration_date) VALUES (?, ?, ?, ?)'
                values = (entity.id, entity.first_name, entity.username,
                          datetime.now(timezone('Europe/Rome')).strftime('%d-%m-%Y %H:%M:%S'))
                db.execute(query, values)
                conn.commit()

            await event.edit(msg, buttons=buttons)

        elif cbaction == 'rdone':
            entity = await bot.get_entity(cbdata)
            query = 'UPDATE users SET registration_date = ? WHERE user_id = ?'
            db.execute(query, (datetime.now(
                timezone('Europe/Rome')).strftime('%d-%m-%Y %H:%M:%S'), entity.id))
            conn.commit()
            msg = 'Perfetto! Ora che hai letto il regolamento possiamo passare alle domande. Queste permetteranno agli admin di conoscerti un po\' meglio prima di decidere se permetterti l\'accesso al gruppo.'
            msg += '\nPuoi inviare le tue risposte in uno o più messaggi. Sticker, immgini, GIF e altri media non verranno registrati come parte delle risposte. Una volta finito premi il pulsante per completare il processo.\n\n'
            msg += 'Ecco le domande:\n'
            questions = readFile()['questions']
            for q in questions:
                msg += '{}) {}\n'.format(q, questions[q])
            buttons = [Button.inline(
                'Fatto!', 'qdone-{}'.format(cbdata).encode())]
            await event.edit(msg, buttons=buttons)

        elif cbaction == 'qdone':
            entity = await bot.get_entity(cbdata)
            query = 'SELECT registration_date FROM users WHERE user_id = ?'
            db.execute(query, (entity.id,))
            res = db.fetchone()
            query = 'SELECT text FROM messages WHERE from_id = ? AND date BETWEEN ? AND ?'
            db.execute(query, (entity.id, res[0], datetime.now(
                timezone('Europe/Rome')).strftime('%d-%m-%Y %H:%M:%S')))
            res = db.fetchall()
            msg_admin = 'Un nuovo utente ha richiesto di entrare nel gruppo!\n'
            msg_admin += '**Utente:** {} (@{})\n'.format(entity.first_name,
                                                         entity.username)
            msg_admin += 'Ecco le risposte che ha fornito per il questionario:\n'
            for r in res:
                msg_admin += '{}\n'.format(r[0])
            query = 'SELECT prev_name, prev_username, last_registration_attempt FROM try_logs WHERE user_id = ? ORDER BY last_registration_attempt LIMIT 1'
            db.execute(query, (entity.id,))
            res = db.fetchone()
            if res:
                msg_admin += '\n\nL\'utente aveva già tentato di registrarsi in data **{}** alle ore **{}**'.format(
                    res[2].split(' ')[0], res[2].split(' ')[1])
                msg_admin += '\nEra precedentemente conosciuto come **{}** (**@{}**)'.format(
                    res[0], res[1])
            buttons = [Button.inline('Accetta', 'accept-{}'.format(entity.id).encode(
            )), Button.inline('Rifiuta', 'decline-{}'.format(entity.id).encode())]
            msg = 'Grazie per aver risposto, non appena un amministratore avrà valutato la tua richiesta ti farò sapere il risultato!'
            await event.edit(msg, buttons=Button.clear())
            await bot.send_message(id_legachan, msg_admin, buttons=buttons)

        elif cbaction == 'accept':
            entity = await bot.get_entity(cbdata)
            msgObj = await event.get_message()

            query = 'UPDATE users SET accepted = ? WHERE user_id = ?'
            db.execute(query, (1, entity.id))
            conn.commit()

            msg_admin = msgObj.message
            msg_admin += '\n\nL\'utente è stato accettato!'
            msg = 'Gli admin hanno acconsentito alla tua richiesta, ecco a te il link di invito!\n'
            link = readFile()['joinlink']
            msg += '[Cliccami!]({})'.format(link)
            await event.edit(msg_admin, buttons=Button.clear())
            await bot.send_message(entity, msg)

        elif cbaction == 'decline':
            entity = await bot.get_entity(cbdata)
            msgObj = await event.get_message()
            msg_admin = msgObj.message
            msg_admin += '\n\nL\'utente è stato rifiutato.'
            msg = 'Mi dispiace ma gli admin hanno rifiutato la tua richiesta :('
            await event.edit(msg_admin, buttons=Button.clear())
            await bot.send_message(entity, msg)

        elif cbaction == 'lost':
            entity = await bot.get_entity(event.original_update.user_id)
            msg = 'Ecco a te il link di ingresso allora: [Cliccami!]({})'.format(readFile()[
                'joinlink'])
            await event.edit(msg)

        elif cbaction == 'notlost':
            msg = 'Hai forse sbagliato a clickare allora?\nPuoi aprire un nuovo menu con il comando /start'
            await event.edit(msg)

        elif cbaction == 'info':
            entity = await bot.get_entity(event.original_update.user_id)
            msg = readFile()['info']
            buttons = [Button.inline(
                '<- Back', 'home-{}'.format(entity.id).encode())]
            await event.edit(msg, buttons=buttons)

        elif cbaction == 'home':
            entity = await bot.get_entity(event.original_update.user_id)
            msg = readFile()['start_msg']
            buttons = [[Button.inline('Vorrei entrare nel gruppo!', 'join-{}'.format(entity.id).encode())], [
                Button.inline('Dammi più informazioni su di te!', 'info'.encode())]]
            await event.edit(msg, buttons=buttons)

###############################################################################
#                   Events handling
#                   Commands
###############################################################################


@bot.on(events.NewMessage)
async def handler(event):
    if event.message.is_private:
        storeMsg(event.message)


# @bot.on(events.NewMessage(pattern=r'^\/getid$'))
# async def handler(event):
#     print(event.chat.id)

@bot.on(events.NewMessage(pattern=r'^\/getuserid'))
async def handler(event):
    if event.message.is_private:
        if event.message.from_id == owner:
            param = event.message.text.split(' ')[1]
            entity = await bot.get_entity(param)
            print(entity.id)


@bot.on(events.NewMessage(pattern=r'^\/updatelink'))
async def handler(event):
    if event.message.is_private:
        if event.message.from_id in admins:
            param = event.message.text.split(' ')[1]
            data = readFile()
            data['joinlink'] = param
            with open('misc.json', 'w') as fh:
                json.dump(data, fh, indent=4, sort_keys=True)
            await event.reply('Link aggiornato con successo!')


@bot.on(events.NewMessage(pattern=r'^\/start$'))
async def handler(event):
    if event.message.is_private:
        messageObj = event.message
        msg = readFile()['start_msg']
        buttons = [[Button.inline('Vorrei entrare nel gruppo!', 'join-{}'.format(event.message.from_id).encode())], [
            Button.inline('Dammi più informazioni su di te!', 'info'.encode())]]
        await event.message.respond(msg, buttons=buttons)


@bot.on(events.NewMessage(pattern=r'^\/discord$'))
async def handler(event):
    if not event.message.is_private:
        with open('misc.json', 'r') as f:
            data = json.load(f)
            msg = data['discord']
        await bot.send_message(event.chat.id, msg, link_preview=False)


###############################################################################
#                   Events handling
#                   Chat actions
###############################################################################
# Greetings and rules on the event of someone joining the group.
@bot.on(events.ChatAction)
async def handler(event):
    if event.chat.id == id_lega:
        if event.user_joined or event.user_added:
            entity = await bot.get_entity(event.action_message.from_id)
            query = "SELECT EXISTS(SELECT 1 FROM users WHERE user_id = ? AND accepted = 1)"
            db.execute(query, (entity.id,))
            res = db.fetchone()
            if res[0]:
                greetings = readfile()["greetings"]
                rules = readfile()["rules"]
                await event.reply(random.choice(greetings).format(username=entity.first_name) + '!\nAssicurati di leggere il regolamento!')
                await bot.send_message(event.chat.id, rules)
            else:
                await event.action_message.delete()
                await bot.send_message(entity, 'Mi dispiace {} ma non sei stato approvato da un amministratore oppure sei stato bannato, non puoi entrare nel gruppo'.format(entity.first_name))
                await bot.kick_participant(event.chat.id, entity.id)
        elif event.user_kicked:
            entity = await bot.get_entity(event.action_message.action.user_id)
            query = "UPDATE users SET accepted = ? WHERE user_id = ?"
            db.execute(query, (2, entity.id))
            msg = 'Sei stato bannato dal gruppo. Se hai domande o hai bisogno di chiarimenti, per favore contatta uno degli admin.'
            await event.action_message.delete()
            await bot.send_message(entity.id, msg)


bot.start()
bot.run_until_disconnected()
